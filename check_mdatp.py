import asyncio
import itertools
import re
from typing import Callable, Optional

import aiohttp


URLS: list[str] = [
    "https://packages.microsoft.com/debian/10/prod/pool/main/m/mdatp/",
    "https://packages.microsoft.com/debian/11/prod/pool/main/m/mdatp/",
    "https://packages.microsoft.com/debian/12/prod/pool/main/m/mdatp/",
]
VER_RE: re.Pattern = re.compile(r'"mdatp_(?P<version>[\d\.]+)[\._]amd64\.deb"')


async def gather_versions(
    url: str, version_extractor: Callable, event: asyncio.Event
) -> list[str]:
    res = []
    async with aiohttp.ClientSession() as s:
        async with s.get(url) as r:
            line: bytes
            async for line in r.content:
                if version := version_extractor(line.decode()):
                    res.append(version)
    event.set()
    return res


async def spinner(events: list[asyncio.Event]):
    seq = itertools.cycle(["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"])
    while True:
        if sum((event.is_set() for event in events)) >= len(events):
            break
        print(f"\r {next(seq)}", end="", sep="", flush=True)
        await asyncio.sleep(0.06)
    print("\r", end="")


def version_extractor(raw: str) -> Optional[str]:
    version = None
    if result := VER_RE.search(raw):
        version = result.groupdict()["version"]
    return version


async def get_installed_version(event: asyncio.Event) -> str:
    version: str = "0.0.0"

    proc = await asyncio.create_subprocess_shell(
        "pacman -Qi mdatp-bin", stdout=asyncio.subprocess.PIPE
    )

    stdout, _ = await proc.communicate()
    for row in stdout.split(b"\n"):
        if b"Version" in row:
            version = row.decode().split(" ")[-1][:-2]
            break

    event.set()
    return version


async def main():
    print("\x1b[?25l\x1b[1m### check mdatp version ###\x1b[22m")
    for url in URLS:
        print(f"[{url}]")

    ev_http = asyncio.Event()
    ev_os = asyncio.Event()
    events: list[asyncio.Event] = [ev_http, ev_os]

    spr = spinner(events)

    gatherers = [
        asyncio.Task(gather_versions(url, version_extractor, ev_http))
        for url in URLS
    ]
    checker = asyncio.Task(get_installed_version(ev_os))

    await asyncio.gather(spr, *gatherers, checker)

    versions = [
        [*[int(e) for e in version.split(".")], version]
        for version in {
            version
            for version in itertools.chain(
                *[gatherer.result() for gatherer in gatherers]
            )
        }
    ]

    versions.sort()
    latest_available: str = "n/a"
    if versions:
        latest_available = versions[-1][-1]

    installed: str = checker.result()

    print(f"* latest available    : {latest_available}")
    print(f"* installed           : {installed}\x1b[?25h")
    if latest_available != "n/a" and latest_available > installed:
        print("\x1b[31mplease update your mdatp package\x1b[39m")


if __name__ == "__main__":
    asyncio.run(main())
